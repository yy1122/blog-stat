package com.blog.spark.mytest

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext};

/**
  * SQLContext的使用
  * IEDA在本地，数据在服务器
  */
object SQLContextApp {

  def main(args: Array[String]): Unit = {

    //val path=args(0)

    //1.创建相应的Context
    val sparkConf = new SparkConf()

    //测试或者生产环境中，AppName和Master我们通常脚本进行设置
    sparkConf.setAppName("SQLContextApp").setMaster("local[2]")

    val sc = new SparkContext(sparkConf)
    val sqlContext = new SQLContext(sc)

    //2.相关业务处理:json
    val people = sqlContext.read.format("json").load("D:\\spark\\people.json")
    people.printSchema()
    people.show()

    //3.关闭资源
    sc.stop()
  }

}
