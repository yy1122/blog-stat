package com.blog.spark.mytest

import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.{SparkConf, SparkContext}
;

/**
  * HiveContext的使用
  */
object HiveContextApp {

  def main(args: Array[String]): Unit = {

    val path=args(0)

    //1.创建相应的Context
    val sparkConf = new SparkConf()

    //测试或者生产环境中，AppName和Master我们通常脚本进行设置
    sparkConf.setAppName("SQLContextApp").setMaster("local[2]")

    val sc = new SparkContext(sparkConf)
    val hiveContext = new HiveContext(sc)

    //2.相关业务处理:json
    hiveContext.table("hive_wordcount").show()

    //3.关闭资源
    sc.stop()
  }

}
