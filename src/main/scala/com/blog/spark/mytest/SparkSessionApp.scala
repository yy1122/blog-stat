package com.blog.spark.mytest;

import org.apache.spark.sql.SparkSession;
/**
  * @description
  * @author yuyon26@126.com
  * @date 2018/10/9 14:09
  */
object SparkSessionApp {

  def main(args: Array[String]): Unit = {

    val  spark=SparkSession.builder().appName("SpartSessionApp").master("local[2]").getOrCreate()

    val people = spark.read.json("D:\\spark\\people.json")
    people.show()

    spark.close()
  }

}
