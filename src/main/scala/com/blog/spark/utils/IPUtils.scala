package com.blog.spark.utils

import com.ggstar.util.ip.IpHelper

/**
  * @description ip解析
  * @author yuyon26@126.com
  * @date 2018/10/5 19:49
  */
object IPUtils {

  def getCity(ip: String) = {
    IpHelper.findRegionByIp(ip)
  }

  def main(args: Array[String]): Unit = {
    //println(getCity("123.147.248.2"))

    val days=20180914 to (20180930)

    days.foreach(println)

  }

}
