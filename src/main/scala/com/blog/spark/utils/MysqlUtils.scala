package com.blog.spark.utils

import java.sql.{Connection, DriverManager, PreparedStatement}

/**
  * @description Mysql操作工具类
  * @author yuyon26@126.com
  * @date 2018/10/6 14:28
  */
object MysqlUtils {

  val url = "jdbc:mysql://127.0.0.1:3306/blog?user=root&password=root&useSSL=false&characterEncoding=utf-8"

  /**
    * 获取数据库连接
    */
  def getConnetction() = {
    DriverManager.getConnection(url)
  }

  /**
    * 释放数据库链接等资源
    * @param connection
    * @param pstmt
    */
  def release(connection: Connection, pstmt: PreparedStatement) = {
    try {
      if (null != pstmt) {
        pstmt.close()
      }
    } catch {
      case e: Exception => e.printStackTrace()
    } finally {
      if (null != connection) {
        connection.close()
      }
    }
  }

  def main(args: Array[String]): Unit = {
    println(getConnetction())
  }

}
