package com.blog.spark.utils

import java.util.{Date, Locale}

import org.apache.commons.lang3.time.FastDateFormat

/**
  * @description
  * @author yuyon26@126.com
  * @date 2018/10/5 15:27
  */
object DateUtils {

  /**
    * 输入文件日期格式：10/Nov/2016:00:00:00 +0800
    */
  val YYYYMMDDHHMM_TIME_FORMAT =FastDateFormat.getInstance("dd/MMM/yyyy:HH:mm:ss Z",Locale.ENGLISH)

  /**
    * 目标日期格式 ：2018-12-12 12:12:12
    */
  val TARGET_FORMAT=FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss")

  def parse(time:String)={
    TARGET_FORMAT.format(new Date(getTime(time)))
  }

  /**
    * 获取输入日志时间
    * @param time
    * @return long
    */
  def getTime(time:String)={
    try{
      YYYYMMDDHHMM_TIME_FORMAT.parse(time.substring(time.indexOf("[")+1,
        time.lastIndexOf("]"))).getTime
    }catch {
      case e:Exception=>{
        0L
      }
    }
  }

  def main(args: Array[String]): Unit = {
    println(parse("[10/Nov/2016:00:00:00 +0800]"))
  }
}
