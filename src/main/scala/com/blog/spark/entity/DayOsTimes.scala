package com.blog.spark.entity

/**
  * @description os平台实体类
  * @author yuyon26@126.com
  * @date 2018/10/6 22:16
  */
case class DayOsTimes(day: String, os: String, times: Long)
