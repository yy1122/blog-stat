package com.blog.spark.entity

/**
  * @description 浏览器统计实体类
  * @author yuyon26@126.com
  * @date 2018/10/6 22:16
  */
case class BrowserOsTimes(day: String, browser: String, times: Long)
