package com.blog.spark.entity

/**
  * @description dayipareatime实体类
  * @author yuyon26@126.com
  * @date 2018/10/6 15:22
  */
case class DayAreaTimes(day: String,  area: String, times: Long)
